package routers

import (
	"github.com/gorilla/mux"
	"log"
	"movie-service/context"
	"movie-service/controller"
	"net/http"
)

func InitializeRouter(appContext *context.AppContext) (*mux.Router, error) {
	router := mux.NewRouter().StrictSlash(true)

	router.Path("/").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.RootController),
	}).Methods("GET")

	router.Path("/movieByMood").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetMovieByMood),
	}).Methods("POST")

	router.Path("/movieOfTheDay").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetMovieOfTheDay),
	}).Methods("GET")

	router.Path("/randomMovie").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetRandomMovie),
	}).Methods("GET")

	router.Path("/undiscoveredMovie").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetUndiscoveredMovie),
	}).Methods("GET")

	router.Path("/movie/{id}").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetMovie),
	}).Methods("GET")

	router.Path("/flushMovieByMood").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.FlushMovieByMood),
	}).Methods("POST")

	router.Path("/topMovies/{page}").Handler(HandlerWrapper{
		AppContext:    appContext,
		CustomHandler: CustomHandler(controller.GetTopMovies),
	}).Methods("GET")

	return router, nil
}

type HandlerWrapper struct {
	AppContext    *context.AppContext
	CustomHandler CustomHandler
}

func (h HandlerWrapper) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	log.Println("requestURI: " + request.RequestURI)
	h.CustomHandler(h.AppContext, writer, request)
}

type CustomHandler func(appContext *context.AppContext, w http.ResponseWriter, r *http.Request)

//func Use(handler CustomHandler, middleware ...func(customHandler CustomHandler) CustomHandler) CustomHandler {
//	for _, m := range middleware {
//		handler = m(handler)
//	}
//	return handler
//}
