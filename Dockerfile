FROM golang:1.16-alpine
RUN apk add build-base
WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

COPY . ./

RUN go build -o /movie-service

EXPOSE 10000

CMD [ "/movie-service" ]