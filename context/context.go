package context

import "database/sql"

type AppContext struct {
	DatabaseContext *DatabaseContext
}

type DatabaseContext struct {
	MovieDatabase Database
}

type Database struct {
	DatabaseHandler *sql.DB
}

func NewAppContext(dbContext *DatabaseContext) *AppContext {
	return &AppContext{DatabaseContext: dbContext}
}

func NewDatabaseContext(movieDatabase Database) *DatabaseContext {
	return &DatabaseContext{MovieDatabase: movieDatabase}
}
