package db

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"movie-service/context"
	"movie-service/models"
	"movie-service/utils"
	"time"
)

func ConnectDB(dbPath string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	db.SetConnMaxLifetime(time.Minute * 5)
	return db, nil
}

type Repository struct {
	AppContext *context.AppContext
}

type RepositoryInterface interface {
	GetMovie(id string) (*models.Movie, error)
	GetTopMovies(id int) ([]models.Movie, error)
	GetMovieCustomized(operationType string) (*models.Movie, error)
}

func NewRepository(appContext *context.AppContext) RepositoryInterface {
	return &Repository{AppContext: appContext}
}

func (r Repository) GetMovie(id string) (*models.Movie, error) {
	fmt.Println(id)
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM Metadata 
				WHERE md_movie_id = %v`
	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(fmt.Sprintf(query, id))
	if err != nil {
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	if len(movies) == 0 {
		return nil, nil
	}
	return &movies[0], nil
}

func (r Repository) GetTopMovies(page int) ([]models.Movie, error) {
	lowerBound := 1
	upperBound := 10
	if page > 1 {
		lowerBound, upperBound = page*10-9, page*10
	}
	query := `	select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date
    			from (
    			select md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date,
     				row_number() over
       				(order by MD_VOTE_AVG DESC, MD_VOTE_CNT DESC ) rn
    				from METADATA
    			    WHERE MD_VOTE_CNT > 1000)
    where rn between %v and %v
    order by rn`

	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(fmt.Sprintf(query, lowerBound, upperBound))
	if err != nil {
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	if len(movies) == 0 {
		return nil, nil
	}
	return movies, nil
}

func (r Repository) GetMovieCustomized(operationType string) (*models.Movie, error) {
	var movie *models.Movie
	var err error
	switch operationType {
	case utils.RandomMovie:
		movie, err = r.getRandomMovie()
	case utils.UndiscoveredMovie:
		movie, err = r.getUndiscoveredMovie()
	case utils.MovieOfTheDay:
		movie, err = r.getMovieOfTheDay()
	}
	if err != nil {
		return nil, err
	}
	return movie, err
}

func (r Repository) getMovieOfTheDay() (*models.Movie, error) {
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM Metadata 
				WHERE md_vote_avg >= 8 AND md_vote_cnt >= 100
				ORDER BY RANDOM() LIMIT 1`

	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	if err != nil {
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}

func (r Repository) getRandomMovie() (*models.Movie, error) {
	query := `SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date FROM Metadata ORDER BY RANDOM() LIMIT 1`
	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	if err != nil {
		return nil, err
	}

	var movies []models.Movie
	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}

func (r Repository) getUndiscoveredMovie() (*models.Movie, error) {
	query := `	SELECT md_movie_id, md_title, md_overview, md_orig_lang, md_poster_path, md_vote_avg, md_release_date 
				FROM Metadata 
				WHERE md_vote_avg >= 6 AND md_vote_avg <= 8 AND md_vote_cnt >= 20
				ORDER BY RANDOM() LIMIT 1`
	rows, err := r.AppContext.DatabaseContext.MovieDatabase.DatabaseHandler.Query(query)
	if err != nil {
		return nil, err
	}

	fmt.Println(&rows)
	var movies []models.Movie

	for rows.Next() {
		var r models.Movie
		err = rows.Scan(&r.Id, &r.Title, &r.Overview, &r.Language, &r.PosterUrl, &r.Rating, &r.ReleasedDate)
		if err != nil {
			log.Fatalf("Scan: %v", err)
			return nil, err
		}
		movies = append(movies, r)
	}
	return &movies[0], nil
}
